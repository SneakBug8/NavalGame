﻿using UnityEngine;

public class Ships : MonoBehaviour {

	public static Ships Global;

	public string[] names = {"Галлеон","Брат"};

	public ShipType[] Types= new ShipType[1];
	public Ship[] sellships = new Ship[3];

	void Awake() {
		Global = this;
		Types[0] = new ShipType {name="Fregate",cannonscount=5,reloadtime=1.0F,cargocapacity=200,peoplecapacity=100,speed=2,maxhealth=250};
	}

	void Start() {
		// sellships[0]=new Ship();
	}
	public float randomizestuff(int source, float modif) {
		return source + Random.Range(-modif, modif) ;
	}
}

public class ShipType {
	/// Название класса кораблей
	public string name;
	/// Максимальное количество пушек
	public int cannonscount;
	// Время перезарядки. {Secs}
	public float reloadtime;
	/// Грузоподъемность. Макс кол-во груза.
	public int cargocapacity;
	/// Максимальная команда.
	public int peoplecapacity;
	/// Скорость корабля
	public int speed;

	public int maxhealth;
}