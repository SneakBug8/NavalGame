﻿using UnityEngine;

public class Trigger : MonoBehaviour {
	public int cityid;
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.Equals(Player.Global.gameObject)) {
		MainController.Global.curcity=Cities.Global.list[cityid];
		GuiController.Global.PortPanel.SetActive(true);
		}
    }
	void OnTriggerExit(Collider other) {
		GuiController.Global.PortPanel.SetActive(false);
    }
}
