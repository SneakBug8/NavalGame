﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour {

	public static Player Global;
	public Vector3 Destination;
	public bool AutoMove;
	NavMeshAgent agent;
	public Ship Ship;

	// public Status status;

	public Resources resources = new Resources();
	void Awake() {
		Global = this;
		Ship = gameObject.GetComponent<Ship>();
		agent=GetComponent<NavMeshAgent>();
	}
	void Start () {
		resources.Init();
		if (Debug.isDebugBuild) {
		for (int i=0; i<9; i++) {resources.list[i].count=10;}
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 movement = new Vector3(0,0,Input.GetAxis("Ship Vertical"));
		Vector3 rotation = new Vector3(0,Input.GetAxis("Ship Rotation"),0);
		if (movement != Vector3.zero || rotation != Vector3.zero) {
			transform.Translate(movement*Time.deltaTime*agent.speed);
			transform.Rotate(rotation*Time.deltaTime*agent.angularSpeed);
			AutoMove=false;
			Destination=Vector3.zero;
		}
		if (AutoMove)
		{
			agent.SetDestination(Destination);
		}
	}

	void OnAttack() {
		AutoMove=false;
	}
}
