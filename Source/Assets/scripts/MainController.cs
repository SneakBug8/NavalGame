﻿using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour {
///Current city of player
public City curcity;
/// Coords before city enter
public Vector3 lastcoords;
public Vector3 rotation;
public static MainController Global;

private bool FirstLoad=true;

public GameObject Canvas;

public Slider HealthSlider;
public Slider ReloadSlider;

private GameObject otherInstance;

public GameObject projectilepref;

	void Awake () {
	Global = this;
	DontDestroyOnLoad(transform.gameObject);
	}
	void Start () {
		// Generating prices and cities first time.
		if (FirstLoad) {Cities.Global.Init(); PricesController.Global.GeneratePrices();}

		otherInstance = GameObject.Find("MainController");
		if (otherInstance!=gameObject) {
			FirstLoad=false;
			Destroy(otherInstance);
		}
	}
	public void Save() {
		/* PlayerPrefs.SetFloat("CoordX",lastcoords.x);
		PlayerPrefs.SetFloat("CoordZ",lastcoords.z);
		PlayerPrefs.SetFloat("RotateY",rotation.y); */
	}

	public void Load() {
		/* lastcoords = new Vector3(PlayerPrefs.GetFloat("CoordX"),0,PlayerPrefs.GetFloat("CoordZ"));
		rotation = new Vector3(0,PlayerPrefs.GetFloat("RotateY"),0);
		Player.transform.Translate(lastcoords);
		Player.transform.Rotate(rotation); */
	}

	public void Update() {
		HealthSlider.maxValue=Player.Global.Ship.Type.maxhealth;
		HealthSlider.value=Player.Global.Ship.health;

		ReloadSlider.maxValue=Player.Global.Ship.Type.reloadtime;
		ReloadSlider.value=Player.Global.Ship.Reloading;
	}
}