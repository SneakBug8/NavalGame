﻿using UnityEngine;

public class Cities : MonoBehaviour {
public static Cities Global;
public City[] list = new City[2];

public GameObject havanatrigger;
public GameObject portroyaltrigger;

void Awake() {
    Global = this;
}

public void Init()
{
    list[0]=new City() {name="Havana",triggerobj=havanatrigger};
    list[1]=new City() {name="Port Royal",triggerobj=portroyaltrigger};
    // list[0].prices.buy
}

}

public class Prices {
    /// Player buy resources
    public int[] buy = {1,1,1,1,1,1,1,1,1};
    /// Player sell resources
    public int[] sell = {1,1,1,1,1,1,1,1,1};
}

public class City {
public string name;
public GameObject triggerobj;
public Prices prices = new Prices();
public Prices modificators = new Prices();
}