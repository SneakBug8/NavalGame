﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class battle : MonoBehaviour {

	private GameObject target;
	private Vector3 destination;
	private NavMeshAgent agent;
	public Ship Ship;

	void Start() {
		Ship=GetComponent<Ship>();
		agent=this.GetComponent<NavMeshAgent>();
	}
	void Update () {
		float distance = Vector3.Distance(transform.position,Player.Global.transform.position);
		if (distance<50) {
			target = Player.Global.transform.gameObject;
		}
		else {
			return;
		}
		if (distance>10) {
			agent.SetDestination(target.transform.position);
		}
		else {
			float angle = Vector3.Angle(transform.TransformDirection(Vector3.forward),target.transform.position);

			if (Physics.Raycast(transform.position,transform.TransformVector(Vector3.right),20)) {
				Ship.Shoot(Vector3.right);
				return;
			}
			if (Physics.Raycast(transform.position,transform.TransformVector(Vector3.left),20)) {
				Ship.Shoot(Vector3.left);
				return;
			}

			transform.Rotate(Vector3.RotateTowards(transform.right,target.transform.position,1,0.0F)*agent.angularSpeed*Time.deltaTime);
		}
	}
}
