﻿using UnityEngine;
using UnityEngine.AI;

public class trade : MonoBehaviour {

	Transform target;
	NavMeshAgent agent;

	public bool Move = true;

	// Use this for initialization
	void Start () {
		agent=GetComponent<NavMeshAgent>();		
	}
	
	// Update is called once per frame
	void Update () {
		if (agent.velocity==Vector3.zero) {
			ChooseTargetCity();
		}
		if (Move)
			agent.SetDestination(target.position);
	}
	/// Make new destination city for ship
	void ChooseTargetCity () {
		int targetid = Random.Range(0,Cities.Global.list.Length);
		target = Cities.Global.list[targetid].triggerobj.transform;
	}
}
