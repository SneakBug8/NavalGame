﻿using UnityEngine;
public class simple : MonoBehaviour {
public float speed = 6.0F;
     public float gravity = 20.0F;
     private Vector3 moveDirection = Vector3.zero;
	 private Vector3 rotateDirection = Vector3.zero;
	 private int timer=0;
 
     void Update() {
		 timer++;
         CharacterController controller = GetComponent<CharacterController>();
         // is the controller on the ground?
         if (controller.isGrounded) {
             //Feed moveDirection with input.
             moveDirection = new Vector3(0, 0, speed);
             if (timer>60) {
			 rotateDirection = Vector3.zero;
             }
             if (timer>360) {
				 timer=0;
				rotateDirection = new Vector3(0,Random.Range(-1, 1),0);
			 }
			 rotateDirection = Quaternion.AngleAxis(-45, Vector3.up) * rotateDirection;
             moveDirection = transform.TransformDirection(moveDirection);
             //Multiply it by speed.
             moveDirection *= speed;
             
         }
         moveDirection.y -= gravity * Time.deltaTime;
         //Making the character move
         controller.Move(moveDirection * Time.deltaTime);
		 transform.Rotate(rotateDirection, Space.World);
     }
}
