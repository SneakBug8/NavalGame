﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ally : MonoBehaviour {
	public NavMeshAgent agent;
	void Start() {
		agent = GetComponent<NavMeshAgent>();
	}
	void Update () {
		if (Vector3.Distance(transform.position,Player.Global.transform.position)>5) {
			agent.SetDestination(Player.Global.transform.position);
		}
	}
}
