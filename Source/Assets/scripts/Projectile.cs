﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

private Vector3 creation;
private Vector3 lastpoint;

void Start() {
	creation = transform.position;
	lastpoint = creation;
}

void OnCollisionEnter(Collision col) {
	Color color = Color.red;
	if (col.gameObject.tag=="Ship")
		color=Color.green;
	Debug.DrawLine(creation,transform.position,color,120);
	Destroy(gameObject);
}

void Update() {
	Debug.DrawLine(lastpoint,transform.position,Color.gray,120);
	lastpoint=transform.position;
}
}
