﻿using UnityEngine;
using UnityEngine.AI;

public class Ship : MonoBehaviour {
	/// Название корабля
	public string shipname;
	public int typenum;
	/// Класс корабля
	public ShipType Type;
	public float health;
	public float Reloading;
	private Vector3 prevpos=Vector3.zero;
	public void Start() {
		Type=Ships.Global.Types[typenum];
		health=Type.maxhealth/2;
		this.gameObject.GetComponent<NavMeshAgent>().speed=Type.speed;
	}
	public void Heal() {
		if (health<Type.maxhealth/2) {
		health+=1*Time.deltaTime;
		}
	}
	public void Update() {
		if (Reloading>0)
			Reloading-=Time.deltaTime;

		Heal();

		if (health<=0) {
			Destroy(this.gameObject);
		}

		Debug.DrawLine(prevpos,transform.position,Color.blue,120);
		prevpos=transform.position;
	}
	public void Damage(float dmg) {
		health-=dmg;
	}

	public void Shoot(Vector3 angle) {
		
		angle.y=0.2F;

		if (Reloading>0) {return;}
		float distancebtwcannons = 2.5f/(float)Type.cannonscount;

		for (int i=0; i<Type.cannonscount; i++) {

			float curdist=(i*distancebtwcannons)-Type.cannonscount/2*distancebtwcannons;
			GameObject projectile = Instantiate(MainController.Global.projectilepref, transform.TransformPoint(angle.x/transform.lossyScale.x,0.25F/transform.lossyScale.y,curdist/transform.lossyScale.z), Quaternion.identity);
			projectile.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(angle)*Random.Range(75,125));
		}

		Reloading=Type.reloadtime;
	}
	
	public void SetRandomName() {
		name=Ships.Global.names[Random.Range(0,Ships.Global.names.Length)];
	}

	void OnCollisionEnter(Collision collision) {
		Damage((Mathf.Abs(collision.impulse.x+collision.impulse.z)*2.5F));
		gameObject.SendMessage("OnAttack", null , SendMessageOptions.DontRequireReceiver);
	}

	void OnCollisionStay(Collision collision) {
		Damage((Mathf.Abs(collision.impulse.x+collision.impulse.z)*1.5F));
		gameObject.SendMessage("OnCollision", null , SendMessageOptions.DontRequireReceiver);
	}
}
