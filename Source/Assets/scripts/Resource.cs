public class Resource {
    public int id;
	public string name;
	public int count=0;
}

public class Resources {
	public static string[] resourcenames = {"Gold","People","Cannons","Wood","Metal","Sugar","Textile","Rum","Tabacco"};
   public Resource[] list = new Resource[9];

   public int Total() {
	   int summ=0;
	   for (int i=0;i<9;i++) {
		   summ+=list[i].count;
	   }
	   return summ;
   }

   public void Init() {
   for(int i=0;i<9;i++) {
			list[i] = new Resource();
			list[i].name=resourcenames[i];
			list[i].id=i;
   		}
	}
}