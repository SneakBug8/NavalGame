﻿using System.Timers;
using UnityEngine;

public class PricesController : MonoBehaviour {
	private static Timer genTimer;
	public int timeBetweenGenerations=360;

	public static PricesController Global;

	void Awake () {
	Global = this;
	}
	public void Start() {
		InvokeRepeating("GeneratePrices", timeBetweenGenerations, timeBetweenGenerations);
	}

	public City GeneratePrice(City city) {
		for (int i=0;i<9;i++) {
			again:
			city.prices.buy[i]=Random.Range(0, 60)+city.modificators.buy[i];
			city.prices.sell[i]=Random.Range(0, 60)+city.modificators.sell[i];
			if (city.prices.buy[i]<city.prices.sell[i]) {
				goto again;
			}
		}
		return city;
	}

	public void GeneratePrices() {
		Debug.Log("Generating prices");
		for (int i = 0; i<Cities.Global.list.Length; i++) {
			Cities.Global.list[i] = GeneratePrice(Cities.Global.list[i]);
		}
	}
}
