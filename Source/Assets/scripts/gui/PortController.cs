﻿using UnityEngine;
using UnityEngine.UI;

public class PortController : MonoBehaviour {

	public Text PortName;
	public Button btn0obj;
	public Text btn0textobj;
	public Button btn1obj;
	public Text btn1textobj;
	public Button btn2obj;
	public Text btn2textobj;
	public Button btn3obj;
	public Text btn3textobj;

	void Start () {
		btn0obj.onClick.AddListener(OpenTrade);
		btn1obj.onClick.AddListener(OpenShipyard);
	}
	void OpenShipyard() {

	}
	void OpenTrade() {
		GuiController.Global.TradePanel.SetActive(true);
	}
}
