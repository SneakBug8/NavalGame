﻿using UnityEngine;
using UnityEngine.UI;

public class OnConfirm : MonoBehaviour {
	void Start () {
		gameObject.GetComponent<Button>().onClick.AddListener(onClick);
	}
	void onClick() {
		TradeController.Global.ConfirmTrade();
	}
}
