﻿using UnityEngine;
using UnityEngine.UI;

public class TradeController : MonoBehaviour {
	public City city;
	public static int resourceid;
	/// Столбец с выбранным ресурсом
	public static bool left;
	public static int count;
	public static TradeController Global;

	public Text costtext;
	public Text goldtext;

	void Awake () {
	Global = this;
	}

	void OnEnable() {
		city  = MainController.Global.curcity;
	}

	void Start()
	{
		Cities.Global.Init();
		gameObject.GetComponent<DrawCount>().DrawAllCount();
		DrawCost();
	}
	public void ConfirmTrade() {
		if (left) {
		if (Player.Global.resources.list[resourceid].count>=count) {
			// Даем игроку деньги
			Player.Global.resources.list[0].count+=count*city.prices.sell[resourceid];
			// Забираем ресурсы
			Player.Global.resources.list[resourceid].count-=count;
		}
		}
		else {
			if (Player.Global.resources.list[0].count>=CountCost()) {
				// if (Player.Global.resources.Total()<Player.Global.capacity) {
				// Забираем у игрока деньги
				Player.Global.resources.list[0].count-=city.prices.buy[resourceid]*count;
				// Даем ресурсы
				Player.Global.resources.list[resourceid].count+=count;
				// }
			}
		}
		gameObject.GetComponent <DrawCount>().DrawAllCount();
		DrawCost();
	}
	public int CountCost() {
		int cost;
		if (left) {
			cost=city.prices.sell[resourceid]*count;
		}
		else {
			cost=city.prices.buy[resourceid]*count;
		}
		return cost;
	}

	public void DrawCost() {
		if (resourceid>-1&&count>-1) {
			costtext.text="Cost: "+CountCost();
		}
		goldtext.text="Gold: "+Player.Global.resources.list[0].count;
	}
}