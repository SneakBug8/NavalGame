﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InputCount : MonoBehaviour {

void Start()
{
	gameObject.GetComponent<InputField>().onValueChanged.AddListener(delegate {onValueChange();});
}

void onValueChange() {
	TradeController.count=Int32.Parse(gameObject.GetComponent<InputField>().text);
	TradeController.Global.DrawCost();
}
}
