﻿using UnityEngine;
using UnityEngine.UI;

public class DrawCount : MonoBehaviour {

	public Text LPeople;
	public Text LCannons;
	public Text LWood;
	public Text LMetal;
	public Text LSugar;
	public Text LTextile;
	public Text LRum;
	public Text LTabacco;
	public void DrawAllCount () {
		DrawText(LPeople,1);
		DrawText(LCannons,2);
		DrawText(LWood,3);
		DrawText(LMetal,4);
		DrawText(LSugar,5);
		DrawText(LTextile,6);
		DrawText(LRum,7);
		DrawText(LTabacco,8);
	}

	void DrawText(Text text, int ResNum) {
		text.text=Player.Global.resources.list[ResNum].name+" "+Player.Global.resources.list[ResNum].count;
	}
}
