﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExitButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Button>().onClick.AddListener(Close);
	}
	void Close() {
		GuiController.Global.TradePanel.SetActive(false);
	}
}