﻿using UnityEngine;

public class CameraScript : MonoBehaviour {

public float speed = 6.0F;

public int maxheight;
public int minheight;
public float zoomspeed;
private Vector3 moveDirection;

public bool FollowPlayer;

     void Awake() {
     }

     void Start() {
     }
 
     void Update() {
             moveDirection = new Vector3(Input.GetAxis("Camera Horizontal"), Input.GetAxis("Camera Vertical"), Input.GetAxis("Mouse ScrollWheel")*zoomspeed);
             if (moveDirection!=Vector3.zero) {
                 transform.Translate(moveDirection*Time.deltaTime*speed);
                 FollowPlayer=false;
             }

             if (transform.position.y>maxheight || transform.position.y<minheight) {
                moveDirection.z=0;
             }

            if (Input.GetKeyDown(KeyCode.Q)) {
                Player.Global.Ship.Shoot(Vector3.left);
            }
            
            if (Input.GetKeyDown(KeyCode.E)) {
                Player.Global.Ship.Shoot(Vector3.right);
            }

            if (Input.GetKeyDown(KeyCode.Space)) {
                FollowPlayer=true;
            }

        if (Input.GetMouseButtonDown(1)) {
            RaycastHit hit;
            Ray ray = this.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit)) {
                Player.Global.Destination= new Vector3 (hit.point.x,1,hit.point.z);
                Player.Global.AutoMove=true;
            }
        }

        if (FollowPlayer) {
            transform.position=new Vector3(Player.Global.transform.position.x,transform.position.y,Player.Global.transform.position.z);
        }
     }
}
